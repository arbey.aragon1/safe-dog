import serial, time
import os
from os.path import join, dirname
from dotenv import load_dotenv
import datetime
import json
import firebase_admin
from firebase_admin import credentials
from firebase_admin import firestore

cred = credentials.Certificate('/home/pi/safe-dog/credentials/serviceAccountKey.json')
firebase_admin.initialize_app(cred)

db = firestore.client()

dotenv_path = join(dirname(__file__), '.env')
load_dotenv(dotenv_path)

print(os.getenv)
arduino = serial.Serial(os.getenv('DEVICE_PORT'), 9600)

jsonDocument = {}

rawString = arduino.readline()
ista = [i.replace("\\r\\n'",'').replace("b'",'').split(':') for i in str(rawString).split(',')]
sonDocument = {**jsonDocument, **dict(lista), 'time': str(datetime.datetime.now())}

timeDoc=str(round(time.time() * 1000))
data={'dataS0':55}
db.collection('data').document(timeDoc).set(data)

print(jsonDocument)

